VW DUpdateTitleBlocks
========================================================================================================================

Semi-automatic update of title blocks.

## BRANCHES ############################################################################################################

See the [DLibrary README](https://bitbucket.org/dieterdworks/vw-dlibrary) for the branches info.

## RELEASE CHECKLIST ###################################################################################################

* Create history branch for previous MAJOR release.
* Update hard-coded version number.
* Commit the new version number.
* Create tag for new version.
